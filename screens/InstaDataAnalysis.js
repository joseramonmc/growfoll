import React from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TouchableOpacity, AsyncStorage } from 'react-native'
import {  AppLoading } from 'expo'
import { TestScreen } from './TestScreen'
import { Header } from '../components/Header'

const { width: WIDTH } = Dimensions.get('window');

export default class InstaDataAnalysis extends React.Component {

    access_token = 'IGQVJVQVJXVGVUczctVUM2YW9rZAkJMSk9IbHJfNk16ZAEhjbHUwQmhYdVZAjT0FGeWdYbXFEbGJoX3BYM1lYLXd1VTc0Y2ZA4dEU5cTlzdEtVVU1FeG1pTGF3TEFJN0s0QzBrMHZASWTBn';
    user_id = '17841410702929771';
    insta_uri = 'https://graph.instagram.com/';

    constructor(props) {
        super(props);
        this.state = {
            introDone: false,
            loaded: false,
            username: '',   
            data: null,
            mediaTypes: [],
            dates: [],
            urls: [],
        };
    }

    introDone = () => {
        this.setState({
            introDone:true
        })
    };
   
    componentDidMount(){
        this.getInstaDetail()
        this.getMediaData()
        this.persistInfo();
    }

    async persistInfo() {
        var items = [['mediaTypes', this.state.mediaTypes], ['urls', this.state.urls], ['introDone',this.state.introDone]];
        try {
            await AsyncStorage.setItem(this.state.username, JSON.stringify(items));
        } catch (error) {
            console.log('Error saving data')
        }
    }

    async getInstaDetail () {
        let generalInstaInfo = 
            await fetch( this.insta_uri  + this.user_id + '?access_token=' + 
            this.access_token + '&fields=account_type, username, media_count');
        let usernameData =  await generalInstaInfo.json();
        this.state.username = usernameData.username;
        this.forceUpdate();
    }

    async getMediaData () {
        let mediaData =
            await fetch(this.insta_uri + this.user_id + '/media/?access_token=' 
            + this.access_token);
        let dataMedia = await mediaData.json();
        this.state.data = dataMedia.data;
        let mediaItems = this.state.data.map(async item => {
            let mediaDetailGet = await fetch(this.insta_uri + item.id 
                + '?fields=media_url,timestamp,media_type&access_token='+ this.access_token);
            let mediaDetailItem = await mediaDetailGet.json();
            return mediaDetailItem;   
        });
        mediaItems = await Promise.all(mediaItems)
        mediaItems.map( e => {
            this.setState({
                mediaTypes: [...this.state.mediaTypes, e.media_type],
                urls : [...this.state.urls, e.media_url],
                dates : [...this.state.dates, e.timestamp]
            })   
        })
        this.state.loaded = true;
        this.forceUpdate();
    }

    render(){
        if (!this.state.loaded) {
            return (
                <AppLoading/>
            );
        }else if(!this.state.introDone){
            return (
                <View style={styles.mainContainer}>
                    <TestScreen isDone={this.introDone} username={this.state.username}/>
                </View>
            );
        }
        return (
            <View style={styles.homeContainer}>
                <Header/>
                <View style={styles.centerArea}>
                    <Text style={styles.titleImage}>Last image</Text>
                    <Image
                        style={styles.logo}
                        source={{
                            uri: this.state.urls[1]
                        }}
                    />
                </View> 
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.btnLogin} >
                        <Text style={styles.textButton}>Photo Analysis</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnLogin} >
                        <Text style={styles.textButton}>Flow Analysis</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnLogin} >
                        <Text style={styles.textButton}>Other Analysis</Text>
                    </TouchableOpacity>
                </View>
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }, textButton: {
        fontSize: 15,
        color: 'rgba(21, 30, 36, 0.9)'
    },
    homeContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    logo: {
        width: 300,
        height: 300,
        maxHeight: '80%',
        maxWidth: '80%',
        borderRadius: 150 / 2,
        overflow: "hidden",
        borderWidth: 2,
        borderColor: '#657580',
        paddingVertical: 20
    },
    centerArea:{
        flex: 1, 
        paddingVertical: 20
    },
    titleImage:{
        fontSize: 20,
        color: '#657580',
        marginVertical: 16,
    },btnLogin: {
        width: WIDTH - 180,
        height: 30,
        borderRadius: 45,
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(255, 199, 110,0.8)',
        marginVertical: 15
    }, textButton: {
        fontSize: 15,
        color: 'rgba(21, 30, 36, 0.9)'
    }, buttonContainer:{
        marginVertical: 10,
         justifyContent: 'center',
         alignItems: 'center',
         paddingVertical: 10
    } 
});