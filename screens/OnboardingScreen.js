import React from 'react'
import { Image, View, StyleSheet } from 'react-native'
import Onboarding from 'react-native-onboarding-swiper'

const OnboardingScreen = props => {
    return(
        <View style={styles.container}>
            <Onboarding
                bottomBarHeight={40}
                transitionAnimationDuration={200}
                onDone={props.startSecondOnboarding}
                pages={[
                    {
                        backgroundColor: '#ffc76e',
                        image: <Image source={require('../assets/onboarding/one.png')} style={{ width: 250, height: 250 }}/>,
                        title: 'Ready to improve?',
                        subtitle: 'This is your first step',
                    },
                    {
                        backgroundColor: '#657580',
                        image: <Image source={require('../assets/onboarding/two.png')} style={{width: 300, height:280}}/>,
                        title: 'How',
                        subtitle: 'We compare your Instagram profile against world top instagramers. Turns information into followers.',
                    },
                    {
                        backgroundColor: '#ffc76e',
                        image: <Image source={require('../assets/onboarding/three.png')} style={{ width: 270, height: 380 }}/>,
                        title: 'We are ready, and you?',
                        subtitle: "First, let us show you how it works",
                    },
                ]}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
});

export default OnboardingScreen;