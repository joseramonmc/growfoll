import React, { Component } from 'react'
import { Text, View, StyleSheet, Picker, Image, Dimensions, TouchableOpacity } from 'react-native'

const { width: WIDTH } = Dimensions.get('window');

export class TestScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            experience: 'Nothing, I need help',
            targetFollowers: '1000',
            dataScieceKnowdelge: 'No',
        }
    }

    setExperience(value){
        this.setState({
            experience: value
        })
    }

    setTargetFollowers(value) {
        this.setState({
            targetFollowers: value
        })
    }

    setDatascienceKnowdelge(value) {
        this.setState({
            dataScieceKnowdelge: value
        })
    }

    async persistInfo() {
        var items = [['experience', this.state.experience], ['targetFollowers', this.state.targetFollowers], ['dataScieceKnowdelge', this.state.dataScieceKnowdelge]];
        try {
            await AsyncStorage.setItem(this.state.username + '-profile', JSON.stringify(items));
        } catch (error) {
            console.log('Error saving profile data')
        }
    }

    render() {
        return (
            <View style={styles.mainContainer}>
                <View style={styles.logo}>
                    <Image source={require('../assets/logo.png')} style={styles.logo}></Image>
                </View>
                <Text style={styles.userNameText}> Hello {this.props.username} </Text>
                <Text style={styles.subUserText}> Please, tell us more about you </Text>
                <View style={styles.pickerContainer} >
                    <Text style={styles.subUserText}>Instagram use frequency</Text>
                    <Picker
                        selectedValue={this.state.experience}
                        style={styles.picker}
                        onValueChange={(itemValue, itemIndex) => this.setExperience(itemValue)}
                    >
                        <Picker.Item label="Nothing, I need help" value="Nothing, I need help" />
                        <Picker.Item label="Ocassionally" value="Ocasionally" />
                        <Picker.Item label="Every day" value="Every day" />
                        <Picker.Item label="Every second" value="Every second" />
                    </Picker>
                    <Text style={styles.subUserText}>Expected future followers</Text>
                    <Picker
                        selectedValue={this.state.targetFollowers}
                        style={styles.picker}
                        onValueChange={(itemValue, itemIndex) => this.setTargetFollowers(itemValue)}
                    >
                        <Picker.Item label="Doesn't matter" value="Doesn't matter" />
                        <Picker.Item label="Thousands" value="Thousands" />
                        <Picker.Item label="Millions" value="Millions" />
                    </Picker>
                    <Text style={styles.subUserText}>Expected future followers</Text>
                    <Picker
                        selectedValue={this.state.dataScieceKnowdelge}
                        style={styles.picker}
                        onValueChange={(itemValue, itemIndex) => this.setDatascienceKnowdelge(itemValue)}
                    >
                        <Picker.Item label="What? No idea" value="What? No idea" />
                        <Picker.Item label="Yes, I'm engineer" value="Yes, I'm engineer" />
                    </Picker>
                </View>
                <TouchableOpacity style={styles.btnLogin} onPress={this.props.isDone}>
                    <Text style={styles.textButton}>Continue</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#657580'
        },
    userNameText:{
        color: 'rgba(255,255,255,0.7)',
        fontSize: 30
    },
    subUserText:{
        color: 'rgba(255,255,255,0.7)',
        fontSize: 17
    },
    pickerContainer:{
        paddingVertical: 20
    },
    picker:{
        height: 50, 
        width: 250, 
        fontSize: 15,
        backgroundColor: 'rgba(255, 199, 110,0.6)',
        borderRadius: 25,
        marginVertical: 20
    }, logo: {
        width: 180,
        height: 180
    }, btnLogin: {
        width: WIDTH - 180,
        height: 30,
        borderRadius: 45,
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: 'rgba(255, 199, 110,0.8)',
    }, textButton: {
        fontSize: 15,
        color: 'rgba(21, 30, 36, 0.9)'
    },
});

