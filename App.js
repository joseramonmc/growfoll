import React, { useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Login from './components/Login';
import InstaDataAnalysis from './screens/InstaDataAnalysis'
import OnboardingScreen from './screens/OnboardingScreen';

export default function App() {

  const[logged, setLogged] =  useState(false);
  const[secondOnboarding, setSecondOnboarding] = useState(false);

  if(!logged)
    return (
      <View style={styles.container}>
        <Login statusLogged={ ()=>setLogged(true)}/>
      </View>
    );
  else if (!secondOnboarding)
    return (
      <View style={styles.container}>
        <OnboardingScreen startSecondOnboarding={() => setSecondOnboarding(true)}/>
      </View>
    );
  else
    return (
      <View style={styles.container}>
        <InstaDataAnalysis/>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
