import React, { Component } from 'react'
import { Text, View, StyleSheet, ImageBackground, Image, TextInput, 
    Keyboard, Dimensions, TouchableWithoutFeedback, TouchableOpacity } from 'react-native'

const {width: WIDTH} = Dimensions.get('window');

const Login = props => {
    return(
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
            <ImageBackground source={require('../assets/backgroundImage.png')} style={styles.mainContainer}>
                <View style={styles.logo}>
                    <Image source={require('../assets/logo.png')} style={styles.logo}></Image>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        style={styles.inputUser}
                        placeholder={'Username'}
                        placeholderTextColor={'rgba(255,255,255,0.40)'}
                        underlineColorAndroid='transparent'
                    />
                    <TextInput
                        style={styles.inputUser}
                        placeholder={'Password'}
                        secureTextEntry={true}
                        placeholderTextColor={'rgba(255,255,255,0.40)'}
                        underlineColorAndroid='transparent'
                    />
                </View>
                <TouchableOpacity style={styles.btnLogin} onPress={props.statusLogged}>
                    <Text style={styles.textButton}>Login</Text>
                </TouchableOpacity>
            </ImageBackground>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    mainContainer:{
        flex:1,
        position: 'absolute',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    gradient:{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        height: 300
    },
    logo:{
        width: 270,
        height: 270
    },
    inputContainer:{
        marginVertical: 30
    },
    inputUser:{
        width: WIDTH - 50,
        height: 30,
        borderRadius: 45,
        fontSize: 15,
        paddingLeft: 45,
        backgroundColor: 'rgba(0,0,0,0.35)',
        color: 'rgba(255,255,255,0.7)',
        marginVertical: 10
    },
    btnLogin:{
        width: WIDTH - 180,
        height: 30,
        borderRadius: 45,
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 40,
        backgroundColor: '#ff8614',   
    },
    textButton: {
        fontSize: 15,
        color: 'rgba(21, 30, 36, 0.9)'
    },
    btnInstagram: {
        width: WIDTH - 180,
        height: 30,
        borderRadius: 45,
        fontSize: 15,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
        backgroundColor: '#45ffef',
    }
});

export default Login;
