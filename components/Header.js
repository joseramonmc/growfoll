import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';

export class Header extends Component {
    constructor(props) {
        super(props);
    }

    render(){
        return (
            <View style={styles.header}>
                <Image source={require('../assets/logo.png')} style={styles.logo}></Image>
                <Text style={styles.headerText}>Growfoll</Text>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        width: '100%',
        height: 60,
        paddingTop: 20,
        paddingVertical:20,
        backgroundColor: '#657580',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerText: {
        color: 'rgba(255,255,255,0.7)',
        fontSize: 18,
        padding:20
    },
    logo: {
        width: 80,
        height: 80
    }
});

export default Header;